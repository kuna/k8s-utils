FROM docker:20.10.1
ENV KUBECTL_VERSION 1.18.8
ENV KUBEDOG_VERSION 0.4.0
ENV KUSTOMIZE_VERSION=v4.5.7
ENV JQ_URL https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
ENV GLIBC_VER 2.31-r0
# Install kubectl and jq
RUN apk add -U openssl curl tar gzip bash ca-certificates git libintl gettext && \
    curl -LS -o /usr/bin/jq ${JQ_URL} && chmod +x /usr/bin/jq && \
    curl -L -o /usr/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x /usr/bin/kubectl && \
#    curl -L -o /usr/bin/kubedog https://dl.bintray.com/flant/kubedog/v${KUBEDOG_VERSION}/kubedog-linux-amd64-v${KUBEDOG_VERSION} && \
#    chmod +x /usr/bin/kubedog && \
    kubectl version --client && \
    curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
# Install aws-cli v2
RUN apk --no-cache add binutils curl  && \
    curl -sL https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub -o /etc/apk/keys/sgerrand.rsa.pub && \
    curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-${GLIBC_VER}.apk && \
    curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-bin-${GLIBC_VER}.apk && \
    apk add --no-cache glibc-${GLIBC_VER}.apk glibc-bin-${GLIBC_VER}.apk && \
    curl -sL https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip && \
    unzip awscliv2.zip && \
    aws/install && \
    rm -rf awscliv2.zip aws /usr/local/aws-cli/v2/*/dist/aws_completer /usr/local/aws-cli/v2/*/dist/awscli/data/ac.index /usr/local/aws-cli/v2/*/dist/awscli/examples && \
    apk --no-cache del binutils  && \
    rm glibc-${GLIBC_VER}.apk && \
    rm glibc-bin-${GLIBC_VER}.apk && \
    rm -rf /var/cache/apk/* && \
    docker --version && \
    aws --version
# Install kustomize
RUN curl -sLO https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz && \
    tar xvzf kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz && \
    mv kustomize /usr/bin/kustomize && \
    chmod +x /usr/bin/kustomize && \
    kustomize version
ENTRYPOINT []
CMD []
